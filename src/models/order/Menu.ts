import type { Product } from "./Product";

export interface Menu {
  id: number;
  name: string;
  products: Product[];
  slug: string;
  style: number;
  thumbnail: string;
  title: string;
}

import type { Items } from "./Items";
export interface Options {
  default_index: number;
  description: string;
  group_id: number;
  items: Items[];
  max: number;
  min: number;
  name: string;
}
